# CHANGELOG

## v2.8.0
 * 🔥 Removed dark mode
 * 🍱 Update backgrounds
 * 🔗 Update bookmarks

## v2.7.0
 * ✨ Add hidden bookmarks to be used in search shortcuts
 * 🚸 Include all bookmarks in search shortcuts

## v2.6.0
 * 🚸 Check for IP address in search box
 * 🔗 Update links
 * 🚸 Search for the word "local" not for the letters "l-o-c-a-l"
 * 🔗 Update links
 * 🔗 Update links
 * 🐛 Fix Qwant url
 * 🔗 Update links
 * 🔗 Update links
 * 👽 Update Qwant url

## v2.5.0
 * 👷 Fix CI
 * 📱 Support smartphones and tablets
 * ✨ Add dark mode for home layout
 * 👷 Fix CI build
 * 🔗 Update Kanboard link
 * ➕ Add Jira, Confluence and Google Drive links
 * ➕ Add TweetDeck and Discord links
 * 🐛 Fix navigator.serviceWorker undefined in private mode
 * 🎨 Update favicons

## v2.4.0
 * ✨ Add search in bookmarks Close #12
 * 🎨 Fix dark mode colors Close #11
 * ✨ Add local keyword Close #10

## v2.3.0
 * 🎨 Crop home background
 * ✨ Use Qwant as search engine
 * ✨ Add dark mode

## v2.2.0
 * ✨ Add link to weather widget
 * 🐛 Fix purged but used CSS classes
 * 🐛 Remove saved mode if not "full" or "light"
 * 🐌 Add PurgeCSS
 * ✨ Add full and light modes
 * 📱 Fix mobile display
 * 🐛 Fix update button
 * 🐛 Fix Todoist link
 * 🏗 Rework service worker routine
 * 🐌 Optimize build size
 * 🏗 Rework PWA stuffs
 * 🔥 Remove manifest.json and icon files
 * ➖ Remove virtual regatta link
 * Add virtual regatta link
 * 🐌 Remove cache for index and service worker
 * 🐌 Remove cache for HTML files
 * Update bookmarks.json
 * Add messenger
 * Update bookmarks.json
 * Add DeepL and JsonCompare logos
 * Update bookmarks
 * Update .gitlab-ci.yml
 * Update .gitlab-ci.yml
 * Revert base url
 * Update bookmarks
 * Update .gitlab-ci.yml to push to FTP
 * Fix base url
 * Update .gitlab-ci.yml
 * Add gitlab-ci file

## v2.1.0
 * Update version to 2.1
 * Add search form

## v2.0.0
 * Update readme
 * Remove external custom font
 * Add all icons to not request favicon from Google
 * Add https redirection
 * Fix Google Agenda image name
 * Move images, add Gmail and Agenda
 * Add list component and new images
 * Set vue rules to recommended
 * Update package.json
 * init

## v1.0.2
 * Update Bootstrap 3 url
 * Fix url
 * Fix #1: missing fonts

## v1.0.1
 * Added portainer logo
 * Update links for work

## v1.0.0
 * First version
