import fs from 'fs'
import sharp from 'sharp'

const INPUT_FOLDER =
  'C:/Users/Sylvain/Images/55888ChristopheLavalle.DynamicTheme_jdggxwd41xcr0!App/Images Windows à la une/'
const OUTPUT_FOLDER = './public/img/backgrounds/'
const SRC_FOLDER = './src/'

// Store file names to create JSON with all files
const inputFiles = []
const outputFiles = []

const resizePromises = []

// Resize to FullHD and convert to webp
async function resizeAndConvert(imageFile) {
  const inputFilename = imageFile.split('.')[0]
  const outputFilename = inputFilename + '.webp'

  // Pass the image you want to manipulate to sharp

  await sharp(INPUT_FOLDER + imageFile)
    // Resize
    .resize(1920, 1080)
    // Convert to webp format
    .webp()
    // name your output file eg. output.jpg
    .toFile(OUTPUT_FOLDER + outputFilename)
    .then(() => outputFiles.push(outputFilename))
    .catch((err) => console.error(err))
}

// Create the JSON file with all background filenames
function createBackgroundList() {
  console.log('Creating backgrounds.json...')
  fs.writeFileSync(SRC_FOLDER + 'content/backgrounds.json', JSON.stringify(outputFiles.sort()))
  console.log('Done!')
}

/**
 * MAIN
 */

// Loop through files to resize and copy
fs.readdirSync(INPUT_FOLDER).forEach(async (file) => {
  // Save these filenames in an array...
  inputFiles.push(file)
})

// ... and start working on it
console.log(`Converting ${inputFiles.length} images...`)
for (let index = 0; index < inputFiles.length; index++) {
  const inputFile = inputFiles[index]
  resizePromises.push(resizeAndConvert(inputFile))
}

Promise.all(resizePromises)
  .then(createBackgroundList)
  .catch((e) => console.error('Failed to process images', e))
