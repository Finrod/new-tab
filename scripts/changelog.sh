#!/bin/bash

# Open file descriptor (fd) 3 for read/write on a text file.
exec 3<> CHANGELOG.md

# Generates changelog tag by tag
# https://stackoverflow.com/a/49047929/3493954
echo "# CHANGELOG" >&3
git tag -l --sort=v:refname | tac | while read TAG ; do
    echo >&3
    if [ $NEXT ];then
        echo "## $NEXT" >&3
    else
        echo "## Current" >&3
    fi
    GIT_PAGER=cat git log --no-merges --format=" * %s" $TAG..$NEXT >&3
    NEXT=$TAG
done
FIRST=$(git tag -l --sort=v:refname | head -1)
echo >&3
echo "## $FIRST" >&3
GIT_PAGER=cat git log --no-merges --format=" * %s" $FIRST >&3

# Generates changelog day by day
# NEXT=$(date +%F)
# echo "# Changelog" >&3
# git log --no-merges --format="%cd" --date=short | sort -u -r | while read DATE ; do
#   echo >&3
#   echo "## $DATE" >&3
#   GIT_PAGER=cat git log --no-merges --format=" * %s" --since="$DATE 00:00" --until="$DATE 23:59" >&3
#   NEXT=$DATE
# done

# Close fd 3
exec 3>&-

# Remove some technical commits
sed -i '/dependencies/d' ./CHANGELOG.md
sed -i '/changelog/d' ./CHANGELOG.md
sed -i '/Release/d' ./CHANGELOG.md
sed -i '/Initial commit/d' ./CHANGELOG.md
