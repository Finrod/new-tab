import js from '@eslint/js'
import prettier from 'eslint-config-prettier'
import vue from 'eslint-plugin-vue'
import globals from 'globals'
import ts from 'typescript-eslint'

// Inspired from https://github.com/Shyam-Chen/Vue-Starter/blob/main/eslint.config.js
// https://github.com/vuejs/eslint-config-typescript/issues/76#issuecomment-2205000127
export default [
  {
    languageOptions: {
      globals: {
        ...globals.browser,
        ...globals.node
      }
    }
  },

  // Ignore must be alone in its own configuration object to be global
  // https://github.com/eslint/eslint/discussions/18304
  {
    ignores: [
      'build/',
      'dist/',
      'public/',
      'coverage/',
      // Generated files
      'script/en_save.ts',
      'src/locales/fr.ts',
      // Docs
      'docs'
    ]
  },

  // JS
  js.configs.recommended,
  {
    rules: {
      'sort-imports': [
        'error',
        {
          ignoreDeclarationSort: true,
          allowSeparatedGroups: true
        }
      ]
    }
  },

  // TS
  ...ts.configs.recommended,
  {
    rules: {
      // Enfore using "import type" for type (and autofix all existing imports 🥰)
      '@typescript-eslint/consistent-type-imports': 'error',

      '@typescript-eslint/no-unused-vars': [2, { args: 'none' }],
      '@typescript-eslint/no-var-requires': 'off'
    }
  },
  {
    files: ['**/tests/unit/**/*.spec.{j,t}s?(x)'],
    rules: {
      '@typescript-eslint/no-unused-vars': 'off',
      '@typescript-eslint/no-explicit-any': 'off',
      // Allow require() to import data in tests
      '@typescript-eslint/no-require-imports': [2, { allow: ['tests-data'] }]
    }
  },

  // Vue
  ...vue.configs['flat/recommended'],
  {
    files: ['*.vue', '**/*.vue'],
    languageOptions: {
      parserOptions: {
        parser: ts.parser
      }
    }
  },
  {
    rules: {
      'vue/multi-word-component-names': 'off',

      // Custom rules from uncategorized category
      'vue/component-name-in-template-casing': ['error', 'PascalCase', { registeredComponentsOnly: false }],
      'vue/match-component-file-name': [
        'error',
        {
          extensions: ['js', 'jsx', 'ts', 'tsx', 'vue'],
          shouldMatchCase: true
        }
      ]
    }
  },

  // Prettier
  prettier
]
