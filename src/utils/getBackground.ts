import { getDayOfYear, getYear } from 'date-fns'

import backgrounds from '@/content/backgrounds.json'

export function getBackground() {
  // Id of the day: <year><day of the year>
  const year = getYear(Date.now())
  const dayOfTheYear = getDayOfYear(Date.now())
  const indexOfTheDay = Number(String(year) + String(dayOfTheYear)) % backgrounds.length

  // Return the background of the day
  return backgrounds[indexOfTheDay]
}
