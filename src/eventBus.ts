/**
 * The event bus allows triggering events anywhere and listening to theses events at any place in the application.
 *
 * Using mitt library: https://github.com/developit/mitt
 *
 * Examples
 * *************
 * import eventBus from '@/eventBus'
 *
 * // Trigger a "start request" event
 * eventBus.emit('request-start', method, url, data, headers, options)
 *
 * // Listen to the event "request-start" in order to start the progress bar animation
 * eventBus.on('request-start', this.startProgress)
 *
 * // Stop listening to request-start event with this particular callback
 * eventBus.off('request-start', this.startProgress)
 *
 */
import type { EventType } from 'mitt'
import mitt from 'mitt'

import type { Bookmark } from './types'

// Typing some events
interface Events extends Record<EventType, unknown> {
  fuzzysearch?: Bookmark
}

export default mitt<Events>()
