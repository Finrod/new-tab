export type BookmarkCategory = {
  name: string
  links: Bookmark[]
}

export type Bookmark = {
  title: string
  url: string
}
