import { createApp } from 'vue'

import './scss/theme.scss'

import App from './App.vue'

createApp(App).mount('#app')
