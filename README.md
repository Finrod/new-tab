# New Tab

> Custom new tab page for browsers

To be used with these kind of plugins:
- [New tab override](https://addons.mozilla.org/fr/firefox/addon/new-tab-override/) (Firefox)
- [New tab redirect](https://chrome.google.com/webstore/detail/new-tab-redirect/icpgjfneehieebagbmdbhnlpiopdcmna/related) (Chrome)
- [Custom new tab URL](https://microsoftedge.microsoft.com/addons/detail/custom-new-tab-url/oeibmbobgpgnbnlbaffdgebpeepfbnhi?hl=fr) (Edge)

## Download favicon

Use Google service: https://t0.gstatic.com/faviconV2?client=SOCIAL&type=FAVICON&fallback_opts=TYPE,SIZE,URL&url=https://vitejs.dev&size=16

## Credits

Application icon by [Nick Roach](https://www.iconfinder.com/iconsets/circle-icons-1)

## License

[MIT](http://opensource.org/licenses/MIT)
